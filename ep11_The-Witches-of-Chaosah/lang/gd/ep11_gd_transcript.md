# Transcript of Pepper&Carrot Episode 11 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 11: Bana-bhuidsichean na Dubh-choimeasgachd

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cìob|1|False|A Pheabar, tha thu ’nad chùis-nàire dhan Dubh-choimeasgachd.
Tìom|2|False|Tha Cìob ceart, chan fhiach bana-bhuidseach na Dubh-choimeasgachd ach ma bhios i ’na h-adhbhar eagail, ùmhlachd is urraim.
Carabhaidh|3|False|... ’s dè bhios tusa ris ach tì is cèicean-cuachaige, fiù dha ’r deamhain* ...
Nòta|4|False|* Faic eapasod 8: Cèilidh co-latha-breith Pheabar.
Peabar|5|True|Ach ...
Peabar|6|False|... a bhana-ghoistidhean ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cìob|4|False|IST do BHEUL !
Carabhaidh|9|False|Ta-daaa!
Tìom|1|True|A Pheabar, tha thu buadhach gun teagamh ach chan eil iar-theachd-aiche eile againn.
Tìom|2|False|’S ann oirnne a tha an t-uallach gun dèanamaid bana-bhuidseach mhosach cheart na Dubh-choimeasgachd dhiot.
Peabar|3|False|Ach... cha toigh leam a bhith mosach! Tha ... tha sin calg-dhìreach an aghaidh gach...
Cìob|5|True|... no bheir sinn gach cumhachd uat!
Cìob|6|False|Agus thèid thu ’nad dhìlleachdan Ceann na Feòraige gòrach mar a b’ àbhaist.
Tìom|7|False|Cumaidh Carabhaidh sùil ort o seo a-mach airson d’ oideachadh agus innsidh i dhuinn mu d’ adhartas.
Fuaim|8|False|Uspag!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carabhaidh|1|False|Tha rìgh ùr Acrain ro chaoimhneil ceanalta leis na h-ìochdarain aige. Feumaidh an t-eagal a bhith aig an t-sluagh air an rìgh.
Carabhaidh|2|False|’S e a’ chiad dleastanas agad gun lorg thu an rìgh ’s gun dèan thu smàdadh air ach am biodh tu ri brath-foill air gun duilgheadas an uairsin.
Carabhaidh|3|False|Bheir bana-bhuidseach cheart na Dubh-choimeasgachd buaidh air an fheadhainn chumhachdach!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|An rìgh ?...
Peabar|2|False|... cho òg sin ?!
Peabar|3|False|Feumaidh gu bheil sinn ’nar co-aoisean ...
Carabhaidh|4|False|Dè ’n dàil a th’ ort ?! Greas ort! Dùisg e, bagair e, thoir an t-eagal air !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Srad
Peabar|2|False|An dithis againn, tha sinn caran coltach ri chèile ...
Peabar|3|True|òg ...
Peabar|4|True|aonarach ...
Peabar|5|False|... prìosanaich ar dàin.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tìom|6|False|Tha tòrr obrach romhainn a Pheabar ...
Urram|8|False|09/2015 – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Neach-aithris|7|False|- Deireadh na sgeòil -
Fuaim|1|False|Brag!
Peabar|2|False|?!
Fuaim|3|False|Lasss !
Tìom|4|True|A’ chiad deuchainn:
Tìom|5|True|Dh’FHÀILLIG i gu lèir!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 502 pàtran a thug taic dhan eapasod seo:
Urram|3|False|https://www.patreon.com/davidrevoy
Urram|2|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd airson an ath-eapasod air
Urram|4|False|Ceadachas : Creative Commons Attribution 4.0 Bun-tùs : ri fhaighinn air www.peppercarrot.com Bathar-bog : chaidh 100% dhen eapasod seo a tharraing le bathar-bog saor Krita 2.9.7, G'MIC 1.6.5.2, Inkscape 0.91 air Linux Mint 17
