# Transcript of Pepper&Carrot Episode 03 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 3: De hemmelige ingredienser

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|Komona centrum; Markedsdag
Pepper|2|False|Godmorgen hr., jeg vil gerne have otte græskarstjerner, tak
Sælger|3|False|Værsgo, det bliver 60 Ko*
Pepper|5|False|Åh nej...
Note|4|False|* Ko = Komonas valutaenhed

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Øhhh... Undskyld, jeg tager alligevel kun fire
Sælger|2|False|Hrmf...
Safran|3|False|Hilsner min gode mand. Klargør venligst to dusiner af det hele til mig. Den fineste kvalitet som sædvanlig.
Sælger|4|False|Det er altid en fornøjelse at betjene Dem, Frøken Safran
Safran|5|False|Næh, der har vi jo Pepper
Safran|6|False|Det ser ud til, at der er gang i forretningen ude på landet?
Pepper|7|False|...
Safran|8|False|Jeg går ud fra, du er ved at forberede ingredienserne til morgendagens eliksirkonkurrence?
Pepper|9|True|... en elikisirkonkurrence?
Pepper|10|False|... I morgen?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Hvor heldigt! Og jeg har stadig en hel dag til forberede mig
Pepper|5|False|Lad os vinde den konkurrence!
Skrift|1|False|Komonas Eliksirkonkurrence
Skrift|2|False|Førstepræmien for den BEDSTE ELIKSIR : 50 000Ko
Skrift|3|False|Azardag, kl. 3 Pinkmoon Store Torv, Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Ah!... Nu ved jeg det!
Pepper|3|False|... det er lige, hvad jeg har brug for!
Pepper|4|True|Carrot!
Pepper|5|False|Gør dig klar! Vi skal ud at lede efter ingredienserne!
Pepper|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Først har jeg brug for et par dugdråber fra de sorte skyer...
Pepper|2|False|... og nogle røde bær fra den hjemsøgte skov...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|... så nogle æggeskaller fra vulkan-dalens føniks...
Pepper|2|False|... og til sidst nogle mælkedråber fra en ung drageko

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Det var det, Carrot. Jeg tror vi har det hele
Pepper|2|True|Det ser...
Pepper|3|False|...perfekt ud
Pepper|4|True|Mmm...
Pepper|5|True|Bedste... Kaffe... Nogensinde!
Pepper|6|False|Det er lige, hvad jeg har brug for, så jeg kan arbejde hele natten og lave den bedste eliksir til morgendagens konkurrence!
Fortæller|7|False|Fortsættes...
Credits|8|False|Script og kunst: David Revoy. Oversættelse: Marie Moestrup og Juan José Segura

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon
Credits|2|False|Denne episode kunne ikke være blevet til uden støtte fra de 93 tilhængere:
Credits|1|False|Pepper&Carrot er helt gratis og open-source ( Creative Commons Kreditering 3.0 Ikke-porteret. Materialet i høj opløsning kan downloades )
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|6|False|Særlig tak til: David Tschumperlé (G'MIC) og Krita Team! Oversættelse og rettelser: Amireeti
Credits|7|False|Denne episode er 100% designet med fri software Krita, G'MIC, og Inkscape på Xubuntu (GNU/Linux)
Pepper|5|False|Tusind tak!
