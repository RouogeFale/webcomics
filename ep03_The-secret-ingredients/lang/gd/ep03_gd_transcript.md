# Transcript of Pepper&Carrot Episode 03 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 3: Na cungaidhean rùnach

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|Baile Chomona; Latha fèille
Peabar|2|False|Latha math dhuibh! Am faigh mi ochd reul-pheapagan?
Reiceadair|3|False|Sin sibh, tha iad 60Co*
Peabar|5|False|...Murt
Nòta|4|False|* Co = aonad de dh’airgead Chomona

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Tha mi duilich, chan urrainn dhomh ceannachd ach a ceithir...
Reiceadair|2|False|Grrr...
Cròch|3|False|Sin thu. An ullaich thu dà dhusan de gach meas dhomh? 'S as àirde càileachd, mar as àbhaist.
Reiceadair|4|False|Tha e 'na thlachd mhòr dhomh ur frithea-ladh, a' mhaighdinn Chròch
Cròch|5|False|Seallaibh, seo Peabar
Cròch|6|False|Na can rium gu bheil tòcadh malairt ann an Lùb a' Gheòidh?
Peabar|7|False|...
Cròch|8|False|Saoil a bheil thu ag ullachadh nan cungaidhean rùnach airson dùbhlan nan deoch a-màireach?
Peabar|9|True|... dùbhlan dheoch ?
Peabar|10|False|... a-màireach ?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|4|False|Nach mi a tha fortanach, tha latha agam airson ullachadh fhathast !
Peabar|5|False|Buannaichim an dùbhlan seo !
Sgrìobhte|1|False|Dùbhlan deoch Chomona
Sgrìobhte|2|False|50 000Co de dhuais AIR AN DEOCH AS FHEÀRR
Sgrìobhte|3|False|Là na Sabaid Aig 3 uairean foiseirigh Ionad baile Chomona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|2|False|Ò ... ! Tha fhios 'am ...
Peabar|3|False|... bidh seo gu feum dhomh dha-rìribh !
Peabar|4|True|A Churrain !
Peabar|5|False|Trobhad, sealgamaid na cungaidhean còmhla
Peabar|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|An toiseach, feumaidh mi cuid a neamhnaidean ceòtha às na neòil dhubha seo ...
Peabar|2|False|... agus dearcan dearga on dhlùth-choille thàthaichte

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|... agus gucagan-uighe o ghleann beinn-theine nan ainneamhag ...
Peabar|2|False|... agus mu dheireadh thall, boinneag bainne o bhò-dhràgon òg

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Sin agad e, a Churrain, saoilidh mi nach eil dad a dhìth orm a-nis
Peabar|2|True|Tha coltas ...
Peabar|3|False|... foirfe air
Peabar|4|True|Mmm ...
Peabar|5|True|Seo an cofaidh ... as fheàrr ... a ghabh mi a-riamh !
Peabar|6|False|Seo na bha a dhìth orm ’s mi airson obair fad na h-oidhche ach am bi an deoch as fheàrr agam aig an dùbhlan a-màireach
Neach-aithris|7|False|Ri leantainn ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Urram|2|False|Cha bhiodh an t-eapasod ann gun taic nam 93 pàtranach agam
Urram|1|False|Tha an dealbh-èibhinn-lìn seo saor air fad 's tùs fosgailte aige ( Creative Commons Attribution 3.0 Unported, tha faidhlichean bun-tùis ri am faighinn a chùm luchdadh a-nuas )
Urram|3|False|https://www.patreon.com/davidrevoy
Urram|6|False|Taing shònraichte do : David Tschumperlé (G'MIC) agus sgioba Krita air fad ! Eadar-theangachadh ⁊ ceartachadh na Beurla : Amireeti
Urram|7|False|Chaidh 100% dhen epasod seo a dhèanamh le innealan saora le tùs fosgailte Krita agus G'MIC air Xubuntu (GNU/Linux)
Peabar|5|False|Mòran taing !
