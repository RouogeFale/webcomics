# Transcript of Pepper&Carrot Episode 16 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 16: The Sage of the Mountain

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffron|9|False|No, no, no and no, Pepper.
Writing|2|True|Saffron
Writing|3|False|Witchcraft
Writing|1|False|★★★
Writing|5|False|BUTCHER
Writing|6|False|15
Writing|4|False|13
Writing|7|False|Star Street
Writing|8|False|Hairdresser
Sound|10|True|Glug
Sound|11|False|Glug

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffron|2|False|Personally, I don't get involved with mediating, not even for a friend. That kind of thing always ends badly.
Saffron|1|True|This is something you need to handle on your own.
Pepper|3|False|P-please Saffron...
Pepper|4|True|I'm not learning anything with my godmothers, all they do is take over my home, complain about their old creaky bones and shout at me no matter what I do...
Saffron|6|True|Listen, you're a witch , start acting like one!
Saffron|8|False|Like a big girl!
Saffron|7|True|Talk to them straight!
Pepper|5|False|You really can't come and help me talk to them about it ... ?
Saffron|9|False|Alright, alright! I suppose I can come with you and talk to them about it.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thyme|1|False|What do you mean, you don't learn anything?!
Thyme|12|False|And you're not even brave enough to come tell us yourself?!
Thyme|14|False|If my back wasn't acting up, I'd give you a good lesson! And not one in witchcraft if you see what I mean!
Cayenne|15|True|Thyme, calm down
Cayenne|16|False|They're right...
Bird|2|True|peep
Bird|3|False|peep
Bird|4|True|peep
Bird|5|False|peep
Bird|6|True|peep
Bird|7|False|peep
Bird|8|True|peep
Bird|9|False|peep
Bird|10|True|peep
Bird|11|False|peep
Sound|13|False|BAM!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|We're not as young as we used to be and now is a good time for a lesson.
Cayenne|2|True|I suggest an audience with
Thyme|5|True|Heh heh!
Thyme|6|True|The Sage?
Thyme|7|False|Aren't they too young to understand his teachings?
Pepper|8|True|Too young for what!?
Pepper|9|False|We're going!
Saffron|10|False|"We"?
Thyme|11|True|Right, here we are,
Thyme|12|True|I'll let you introduce yourselves to the Sage.
Thyme|13|False|He can be found at the top of this slope.
Thyme|14|True|Ready to learn
Thyme|15|False|a real lesson?
Cayenne|3|True|the Sage of the mountain
Cayenne|4|False|His counsel will be vital...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ready!
Saffron|2|False|Me too!
Thyme|3|False|Great! Good attitude!
Pepper|9|False|ATTACK !!!
Saffron|6|False|A... A MONSTER !
Pepper|8|True|IT'S A TRAP !
Pepper|7|True|I KNEW IT !!
Pepper|5|False|Oh no !
Thyme|4|False|Go !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|GRAVITAS SPIRALIS !
Saffron|2|False|BRASERO INTENSIA !
Sound|3|False|Splash!
Thyme|4|True|Ahh, you young ones, always wanting to attack everything!
Pepper|7|False|What are these crazy lessons!
Credits|11|False|License: Creative Commons Attribution 4.0, Software: Krita, Blender, G'MIC, Inkscape on Ubuntu
Credits|10|False|Based on the Hereva universe created by David Revoy with contributions from Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Narrator|8|False|- FIN -
Credits|9|False|04/2016 - www.peppercarrot.com - Art & Scenario : David Revoy - English Translation : Alex Gryson
Thyme|6|False|When nothing's going as you'd like it to, nothing beats a good bath! It's good for our rheumatism and your nerves!
Thyme|5|True|The lesson of the Sage is to imitate him!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 671 Patrons:
Credits|2|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
