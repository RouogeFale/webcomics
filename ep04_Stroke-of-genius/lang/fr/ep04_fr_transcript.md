# Transcript of Pepper&Carrot Episode 04 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 4 : Éclair de génie

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|False|La veille du concours de potion, 2h00 du mat' ...
Pepper|2|True|Enfin ... Avec cette potion personne , même pas Safran, ne m'arrivera à la cheville !
Pepper|3|False|J'ai seulement besoin de la tester ...
Pepper|4|False|CAAAAAAAA~ ~AAAROTTE !!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|14|False|CHAT MIAM
Pepper|1|False|Carrot ?
Pepper|2|False|Incroyable !... même pas sous le lit ?
Pepper|4|False|Carrot ?
Pepper|5|False|Carrot ?!
Pepper|6|False|Carrot ?!!!
Pepper|3|False|Carrot !
Pepper|7|False|Je ne pensais pas devoir en arriver là et utiliser cette technique ...
Pepper|8|False|Carrot !
Son|9|True|Crrr
Son|10|True|Crrr
Son|11|True|Crrr
Son|12|True|Crrr
Son|13|False|Crrr
Son|15|True|Shhh
Son|16|True|Shhh
Son|17|True|Shhh
Son|18|True|Shhh
Son|19|False|Shhh
Son|21|False|Shhh Shhh Shhh Shhh Shhh
Son|20|False|Crrr Crrr Crrr Crrr Crrr
Son|23|False|Crrr Crrr Crrr Crrr Crrr
Son|24|False|Shhh Shhh Shhh Shhh Shhh
Carrot|25|False|Gargblr
Son|26|False|Zoooouuuu !
Carrot|22|False|O Sole Miaouuuoo !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|ooooh, Carrot ! t'es trop minou d'être venu tout seul
Pepper|2|False|Pile poil au moment où j'ai besoin de mon assistant préféré
Pepper|3|True|Taaadaaaa ! Je te présente mon chef-d'œuvre
Pepper|4|False|la potion des Génies
Pepper|5|False|juste une petite gorgée ...
Pepper|6|False|... et tu devrais être frappé d'un éclair de génie

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Poc
Pepper|3|False|ouaouh ... Carrot ! c'est fantastique...
Pepper|5|False|tu... ... tu écris ?
Pepper|6|False|E ?
Pepper|7|False|Energie ?
Pepper|8|False|Eternel ?
Pepper|9|False|Emotion ?
Son|4|False|Shrrrrrrr
Son|2|False|Shrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Quoi ?
Pepper|2|False|Embué ?
Pepper|3|False|Emané ?
Pepper|4|False|Allez Carrot, fait un effort, ça ne veut rien dire tout ça
Pepper|5|False|... ça ne veut rien dire tout ça ...
Pepper|6|True|Grrrrrrr !!!
Pepper|7|False|Rien ne marche jamais avec moi !
Pepper|8|True|Bof ...
Pepper|9|False|il me reste encore du temps pour en préparer une meilleure
Son|10|False|CLAShshShshShh !
Pepper|11|False|... bonne nuit Carrot ...
Narrateur|12|False|à suivre ...
Crédits|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo and special thanks to Amireeti for helping me with english corrections !
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat des lecteurs, pour cet épisode, merci aux 156 Mécènes :
Crédits|6|False|https://www.patreon.com/davidrevoy
Crédits|5|False|Vous aussi, devenez mécène de Pepper&Carrot pour le prochain épisode :
Crédits|9|False|Tools : Cet épisode a été dessiné a 100% avec des logiciels libres Krita, G'MIC, Blender, GIMP sur Ubuntu Gnome (GNU/Linux)
Crédits|8|False|Open-source : toutes les sources, polices d'écritures, fichiers avec calques sont disponibles sur le site officiel au téléchargement.
Crédits|7|False|License : Creative Commons Attribution à 'David Revoy' vous pouvez modifier, repartager, vendre, etc...
Carrot|4|False|Merci !
