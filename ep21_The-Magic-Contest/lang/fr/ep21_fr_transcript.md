# Transcript of Pepper&Carrot Episode 21 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 21 : Le Concours de Magie

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|1|False|À la demande générale, la ville de Komona présente le Concours de Magie
Écriture|2|False|Grand Prix de 50 000Ko
Écriture|3|False|Pour la plus impressionnante démonstration magique ! Entrée : 150Ko Nouvelle arène géante et sécurisée Komona Azarday, 10 AirMoon
Écriture|4|False|INVITATION SPÉCIALE
Pepper|5|False|Je suis prête ! Allez, Carrot. On y va !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Toujours certaines de ne pas vouloir venir avec nous ?
Cayenne|2|False|Certaines. On fait de la sorcellerie à Chaosah, pas du spectacle !
Écriture|3|False|Le KOMONAN
Écriture|4|False|INAUGURATION DE LA NOUVELLE ARÈNE AUJOURD'HUI
Écriture|5|False|DES HABITANTS INQUIETS POUR LEURS IMPÔTS
Écriture|6|False|VENTE À GUICHET FERMÉ : LE SUCCÈS AU RENDEZ-VOUS !
Écriture|7|False|TENSIONS POLITIQUES EN TERRE DE AH
Écriture|8|False|AH OCCUPE À PRÉSENT 80% DES TERRITOIRES
Écriture|9|False|VOUS AVEZ UNE BONNE VUE !
Pepper|10|True|Merci de me laisser participer !
Pepper|11|True|Je gagnerai ! Promis !
Pepper|12|False|À ce soir !
Cayenne|14|False|...
Son|13|False|Vlan !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
Audience|2|False|Clap
Maire de Komona|1|False|En tant que Maire de Komona, je déclare le second Grand Concours de Magie ouvert !
Maire de Komona|3|True|Je sais que vous attendiez cet événement avec impatience !
Maire de Komona|4|False|Merci d'avoir fait le déplacement depuis les quatre coins d'Hereva pour découvrir notre grande arène ultra-sécurisée !
Maire de Komona|5|False|Passons maintenant à la présentation des participantes !
Carrot|6|True|Z
Carrot|7|True|Z|nowhitespace
Carrot|8|False|Z ...|nowhitespace
Pepper|9|True|Carrot ?! Mais qu'est-ce que tu fais ?!
Pepper|10|True|Réveille-toi ! Ça commence !
Pepper|11|False|Et tout le monde nous regarde !
Son|12|False|Pok !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
Audience|3|False|Clap
Maire de Komona|1|True|Adepte de la magie mortellement revivifiante de "Zombiah", nous avons l'honneur d'accueillir...
Maire de Komona|2|False|CORIANDRE !
Maire de Komona|4|True|La faune et la flore n'ont aucun secret pour cette sorcière d'"Hippiah" ! Veuillez applaudir...
Maire de Komona|5|False|CAMOMILLE !
Maire de Komona|6|False|Et maintenant, grâce aux prouesses techniques de Komona, voici, en exclusivité mondiale...
Son|7|False|Flop !
Maire de Komona|8|True|...une sorcière des fonds marins maîtrisant la redoutable magie d'"Aquah" !
Maire de Komona|9|False|SPIRULINE !
Pepper|10|False|Aquah ?! Ici ?! Incroyable !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
Audience|9|False|Clap
Carrot|1|False|Gro o o o|nowhitespace
Pepper|2|True|Carrot !
Pepper|3|True|C'est bientôt notre tour !
Pepper|4|True|Un peu de sérieux !
Pepper|5|True|Par pitié !
Pepper|6|False|Juste pour aujourd'hui !
Maire de Komona|7|True|Sorcière de la magie spirituelle et ascétique de "Ah", c'est un plaisir d'avoir parmi nous...
Maire de Komona|8|False|SHICHIMI !
Pepper|10|False|Carrot ! NON !!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
Audience|7|False|Ha !
Maire de Komona|1|False|À présent, voici le tour de cette mystérieuse participante, qui s'est inscrite anonymement pour représenter la flamboyante magie de "Magmah"...
Maire de Komona|3|True|OH ! Quelle surprise ! Oui, c'est bien elle ! Il s'agit de...
Maire de Komona|4|False|SAFRAN !
Son|2|False|Flap !
Maire de Komona|6|False|Ah ! Il semblerait qu'une participante ait déjà un problème !
Son|5|False|Plouf !
Maire de Komona|8|False|Mademoiselle !! Veuillez retourner à votre place !!
Pepper|9|False|Caaarrott ! NON !
Son|10|False|Dzing !
Pepper|11|False|?!
Son|12|False|Splash !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ha-ha ! Merci, Spiruline ! Et désolée...
Pepper|2|True|Où vas-tu, toi ?!
Pepper|3|False|REVIENS !
Maire de Komona|4|True|Hem ! Voici donc notre dernière participante, de la magie de... euh... l'inqualifiable "Chaosah"...
Maire de Komona|5|False|PEPPER !
Maire de Komona|6|False|En tant que grande gagnante du concours de potion, nous avons décidé de lui offrir une place de choix !
Pepper|7|False|?!...
Maire de Komona|8|False|En effet, elle fera...
Maire de Komona|9|False|... partie de notre jury !
Son|10|False|Bam
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|False|Clap
Écriture|16|False|Concours de Magie Jury Officiel
Pepper|17|False|Youpi.
Écriture|18|False|Queen Aiel
Écriture|19|False|Pepper
Écriture|20|False|Lord Azeirf
Narrateur|21|False|À SUIVRE...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|02/2017 - www.peppercarrot.com - Dessin & Scénario : David Revoy
Crédits|2|False|Brainstorming : Craig Maloney, Quiralta, Nicolas Artance, Talime et Valvin.
Crédits|3|False|Amélioration des dialogues : Craig Maloney, Jookia, Nicolas Artance et Valvin.
Crédits|4|False|Remerciements : l'équipe d'Inkscape et particulièrement Mc.
Crédits|5|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Crédits|6|False|Logiciels : Krita 3.2.1, Inkscape 0.91 sur Linux Mint 18.1 XFCE
Crédits|7|False|Licence : Creative Commons Attribution 4.0
Crédits|9|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 816 Mécènes :
Crédits|8|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
