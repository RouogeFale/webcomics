# Transcript of Pepper&Carrot Episode 06 [cs]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|6. díl: Lektvarová soutěž

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sakra, zase jsem usnula s otevřeným oknem...
Pepper|2|True|...to je průvan...
Pepper|3|False|...a jaktože je z okna vidět Komona?
Pepper|4|False|KOMONA!
Pepper|5|False|Lektvarová soutěž!
Pepper|6|True|Já asi včera při práci usnula!
Pepper|9|True|...moment?
Pepper|10|False|Kde to jsem?!?
Bird|12|False|áK?|nowhitespace
Bird|11|True|kv|nowhitespace
Pepper|7|False|*
Note|8|False|* Viz Epizoda 4 : Kapka geniality

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Karotko! Ty jsi fakt zlatíčko, že tě napadlo odvézt mě na soutěž!
Pepper|3|False|Vý-bor-ně!
Pepper|4|True|Dokonce jsi nezapomněl ani vzít lektvar, moje šaty a klobouk...
Pepper|5|False|...takže co jsi vybral za lektvar...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|COŽE?!!
Mayor of Komona|3|False|Jako starosta Komony prohlašuji lektvarovou soutěž... za zahájenou!
Mayor of Komona|4|False|S radostí v našem městě vítám hned čtyři čarodějky, které se přihlásily do prvního ročníku.
Mayor of Komona|5|True|Prosím o
Mayor of Komona|6|False|pořádný potlesk:
Writing|2|False|Komonská lektvarová soutěž

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Tlesk
Mayor of Komona|1|True|Je nám ctí uvítat soutěžící ze slavné Unie Technologů, kterou reprezentuje okouzlující a vynalézavá
Mayor of Komona|3|True|...nezapomeňmě ani na naši místní čarodějku, Komonu zastupuje
Mayor of Komona|5|True|...naše třetí soutěžící k nám přijela ze země zapadajících měsíců
Mayor of Komona|7|True|...a nakonec naše poslední soutěžící k nám přijela z lesa u Veverčí Lhoty
Mayor of Komona|2|False|Koriandra!
Mayor of Komona|4|False|Šafránka!
Mayor of Komona|6|False|Šičimi!
Mayor of Komona|8|False|Feferonka!
Mayor of Komona|9|True|Nechť soutěž započne!
Mayor of Komona|10|False|Vítěze určí potleskoměr!
Mayor of Komona|11|False|Jako první nám svůj lektvar předvede Koriandra
Coriander|13|False|...už se nemusíte bát smrti díky mému...
Coriander|14|True|...Lektvaru
Coriander|15|False|ZOMBIFIKACE!
Audience|16|True|Tlesk
Audience|17|True|Tlesk
Audience|18|True|Tlesk
Audience|19|True|Tlesk
Audience|20|True|Tlesk
Audience|21|True|Tlesk
Audience|22|True|Tlesk
Audience|23|True|Tlesk
Audience|24|True|Tlesk
Audience|25|True|Tlesk
Audience|26|True|Tlesk
Audience|27|True|Tlesk
Audience|28|True|Tlesk
Coriander|12|False|Dámy a pánové...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|NEUVĚŘITELNÉ!
Audience|3|True|Tlesk
Audience|4|True|Tlesk
Audience|5|True|Tlesk
Audience|6|True|Tlesk
Audience|7|True|Tlesk
Audience|8|True|Tlesk
Audience|9|True|Tlesk
Audience|10|True|Tlesk
Audience|11|True|Tlesk
Audience|12|True|Tlesk
Audience|13|True|Tlesk
Audience|14|True|Tlesk
Audience|15|True|Tlesk
Audience|16|False|Tlesk
Saffron|18|True|Teď uvidíte
Saffron|17|True|...ale notak, šetřte si potlesk, lidé Komony!
Saffron|22|False|...vyvolá v nich závist!
Saffron|19|True|MŮJ
Saffron|25|False|ELEGANCE!
Saffron|24|True|...Lektvaru
Saffron|23|False|...abyste toho všeho dosáhli, stačí použít jedinou kapku mého...
Audience|26|True|Tlesk
Audience|27|True|Tlesk
Audience|28|True|Tlesk
Audience|29|True|Tlesk
Audience|30|True|Tlesk
Audience|31|True|Tlesk
Audience|32|True|Tlesk
Audience|33|True|Tlesk
Audience|34|True|Tlesk
Audience|35|True|Tlesk
Audience|36|True|Tlesk
Audience|37|True|Tlesk
Audience|38|True|Tlesk
Audience|39|True|Tlesk
Audience|40|False|Tlesk
Mayor of Komona|42|False|Na tomto lektvaru by mohla zbohatnout celá Komona!
Mayor of Komona|41|True|Úžasné! Neuvěřitelné!
Audience|44|True|Tlesk
Audience|45|True|Tlesk
Audience|46|True|Tlesk
Audience|47|True|Tlesk
Audience|48|True|Tlesk
Audience|49|True|Tlesk
Audience|50|True|Tlesk
Audience|51|True|Tlesk
Audience|52|True|Tlesk
Audience|53|True|Tlesk
Audience|54|True|Tlesk
Audience|55|True|Tlesk
Audience|56|True|Tlesk
Audience|57|True|Tlesk
Audience|58|True|Tlesk
Audience|59|True|Tlesk
Audience|60|False|Tlesk
Mayor of Komona|2|False|Koriandra tímto zá-zrač-ným lektvarem porazila samotnou smrt!
Saffron|21|True|Ten pravý lektvar, na který jste všichni čekali: ten, který ohromí vaše sousedy...
Mayor of Komona|43|False|Váš potlesk hovoří za vše, Koriandra už nemá šanci vyhrát.
Saffron|20|False|lektvar!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Předchozí ukázka nastavila pro Šičimi laťku dost vysoko!
Shichimi|4|True|NE!
Shichimi|5|True|to nejde, je to moc nebezpečné
Shichimi|6|False|PROMIŇTE!
Mayor of Komona|3|False|...no tak, Šičimi, všichni už čekají
Mayor of Komona|7|False|Zdá se, dámy a pánové, že Šičimi soutěž vzdala...
Saffron|8|False|Dej to sem!
Saffron|9|False|...a přestaň si hrát na stydlivku, akorát nám všem kazíš představení
Saffron|10|False|Všichni už vědí, že jsem soutěž vyhrála, takže je jedno, co ten tvůj lektvar dělá...
Shichimi|11|False|!!!
Sound|12|False|V Ž Ž Ž Ů Ů ŮM|nowhitespace
Shichimi|15|False|OBŘÍCH PŘÍŠER!
Shichimi|2|False|Já... já netušila, že tu budeme lektvary předvádět
Shichimi|13|True|POZOR!!!
Shichimi|14|True|To je lektvar

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|KRÁ-KRÁ-K rr rr áá áá á á áá|nowhitespace
Sound|2|False|BUCH!
Pepper|3|True|...paráda!
Pepper|5|False|...můj lektvar vás aspoň trochu pobaví, protože...
Pepper|4|False|takže teď můžu já?
Mayor of Komona|6|True|Uteč, ty huso!
Mayor of Komona|7|False|Soutěž skončila! ...zachraň se!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...jako vždycky, sotva přijdu na řadu, všichni utečou
Pepper|1|True|tak vidíš...
Pepper|4|True|Aspoň už mě napadlo, co můžu udělat s tím tvým "lektvarem", Karotko
Pepper|5|False|...dáme všechno do pořádku a poletíme domů!
Pepper|7|True|Ty
Pepper|8|False|obří elegantní zombie kanárku!
Pepper|10|False|Chceš si dát ještě poslední lektvar?...
Pepper|11|False|...moc ne, co?
Pepper|6|False|HÉJ!
Sound|9|False|P R ÁS K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Hele, pořádně si přečti etiketu...
Pepper|2|False|...Jestli fofrem nezmizíš z Komony, tak to na tebe všechno vyleju!
Mayor of Komona|3|True|Za záchranu našeho města před zničením
Mayor of Komona|4|False|udělujeme první cenu Feferonce za její lektvar...??!!
Pepper|7|False|...no... on to vlastně není tak úplně lektvar, ale vzorek moči mého kocoura na prohlídku u veterináře!
Pepper|6|True|...Hehe! jupí!...
Pepper|8|False|...takže nemusím předvádět?...
Narrator|9|False|Epizoda 6 : Lektvarová soutěž
Narrator|10|False|KONEC
Writing|5|False|50 000 Komů
Credits|11|False|Březen 2015 - Kresba a příběh: David Revoy - Překlad: Martin Doucha

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot je naprosto svobodný open-source komiks sponzorovaný laskavými fanoušky. Za tuto epizodu děkujeme následujícím 245 fanouškům:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|I vy můžete podpořit další epizodu Pepper&Carrot:
Credits|7|False|Nástroje: K vytvoření této epizody byly použity jen 100% svobodné nástroje Krita na Linux Mint
Credits|6|False|Open-source: veškeré zdrojové soubory s vrstvami ve vysokém rozlišení včetně fontů jsou k dispozici ke stažení na oficiální stránce.
Credits|5|False|Licence: Creative Commons Uveďte původ Můžete dělat odvozená díla, úpravy, kopírovat, prodávat atd...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
