# Transcript of Pepper&Carrot Episode 06 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 6: Perlombaan ramuan

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Kemarin aku tertidur dengan jendela terbuka, lagi...
Pepper|2|True|... dingin sekali...
Pepper|3|False|... dan aku bisa melihat Komona dari jendela?
Pepper|4|False|KOMONA!
Pepper|5|True|Perlombaan ramuan!
Pepper|6|True|Aduh, aku ketiduran karena mempersiapkan ramuan!
Pepper|9|True|... tapi?
Pepper|10|False|Di mana aku?!?
Bird|12|False|kwek?|nowhitespace
Pepper|7|False|*
Note|8|False|* Lihat Episode 4 : Ramuan kecerdasan

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Terima kasih karena sudah membawaku ke tempat perlombaan!
Pepper|3|False|Luar biasa!
Pepper|4|True|Kamu juga membawakan pakaian dan ramuanku...
Pepper|5|False|... hmm, ini ramuan apa ya...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|APA INI?!!
Mayor of Komona|3|False|Sebagai walikota Komona, saya akan memulai perlombaan ramuan ini!
Mayor of Komona|4|False|Saya juga mengucapkan terima kasih kepada empat penyihir yang ikut serta dalam perlombaan ini!
Mayor of Komona|5|True|Berikan kepada mereka tepuk tangan yang...
Mayor of Komona|6|False|meriah
Writing|2|False|Perlombaan ramuan Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Prok
Mayor of Komona|1|True|Peserta pertama yang berasal dari Jakarta, dengan hormat kita sambut...
Mayor of Komona|3|True|... peserta kedua berasal dari Komona, kita sambut...
Mayor of Komona|5|True|... peserta ketiga berasal dari ibukota Parahyangan, Bandung...
Mayor of Komona|7|True|... dan peserta terakhir, dari hutan yang sangat dalam di Jonggol, kita sambut...
Mayor of Komona|2|False|Coriander !
Mayor of Komona|4|False|Saffron !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Pepper !
Mayor of Komona|9|True|Perlombaan dimulai!
Mayor of Komona|10|False|Pemenang akan ditentukan dari banyaknya tepuk tangan
Mayor of Komona|11|False|Dimulai dari Coriander, dipersilakan...
Coriander|13|False|... dengan ramuan ini, burung ini bisa...
Coriander|14|True|... menjadi
Coriander|15|False|ZOMBIE!
Audience|16|True|Prok
Audience|17|True|Prok
Audience|18|True|Prok
Audience|19|True|Prok
Audience|20|True|Prok
Audience|21|True|Prok
Audience|22|True|Prok
Audience|23|True|Prok
Audience|24|True|Prok
Audience|25|True|Prok
Audience|26|True|Prok
Audience|27|True|Prok
Audience|28|True|Prok
Coriander|12|False|Halo! semuanya...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|LUAR BIASA !
Audience|3|True|Prok
Audience|4|True|Prok
Audience|5|True|Prok
Audience|6|True|Prok
Audience|7|True|Prok
Audience|8|True|Prok
Audience|9|True|Prok
Audience|10|True|Prok
Audience|11|True|Prok
Audience|12|True|Prok
Audience|13|True|Prok
Audience|14|True|Prok
Audience|15|True|Prok
Audience|16|False|Prok
Saffron|18|False|di sini sudah ada ramuan terbaik
Saffron|17|True|... sebaiknya simpan tepuk tangan Anda, rakyat Komona !
Saffron|20|False|... tidak ada apa-apanya!
Saffron|23|False|BERGAYA!
Saffron|22|True|... lebih
Saffron|21|True|... hanya dengan setetes, dapat menjadikan burung ini...
Audience|26|True|Prok
Audience|25|True|Prok
Audience|24|True|Prok
Audience|28|True|Prok
Audience|29|True|Prok
Audience|30|True|Prok
Audience|31|True|Prok
Audience|32|True|Prok
Audience|33|True|Prok
Audience|34|True|Prok
Audience|35|True|Prok
Audience|36|True|Prok
Audience|37|True|Prok
Audience|38|True|Prok
Audience|39|False|Prok
Mayor of Komona|41|False|Ini dapat membuat Komona jadi kaya!
Mayor of Komona|40|True|Luar biasa! Menakjubkan!
Audience|43|True|Prok
Audience|44|True|Prok
Audience|45|True|Prok
Audience|46|True|Prok
Audience|47|True|Prok
Audience|48|True|Prok
Audience|49|True|Prok
Audience|50|True|Prok
Audience|51|True|Prok
Audience|52|True|Prok
Audience|53|True|Prok
Audience|54|True|Prok
Audience|55|True|Prok
Audience|56|True|Prok
Audience|57|True|Prok
Audience|58|True|Prok
Audience|59|False|Prok
Mayor of Komona|2|False|Coriander dapat membuat burung itu hidup kembali dengan ramuannya!
Saffron|19|True|Ini adalah ramuan yang sesungguhnya: satu-satunya yang dapat membuat peserta lain...
Mayor of Komona|42|False|Dengan ini, maka Coriander langsung tereleminasi!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Apakah Shichimi bisa mengalahkan peserta sebelumnya, silakan dimulai...
Shichimi|4|True|TIDAK!
Shichimi|5|True|Tidak bisa, ini terlalu berbahaya
Shichimi|6|False|MAAF!
Mayor of Komona|3|False|... ayolah Shichimi, semua orang sedang menunggumu
Mayor of Komona|7|False|Kelihatannya Shichimi sedang tidak percaya diri...
Saffron|8|False|Berikan padaku!
Saffron|9|False|... kamu tak usah malu, karena kamu akan menghancurkan acara ini
Saffron|10|False|Semua orang tahu bahwa aku akan memenangkan perlombaan ini...
Shichimi|11|False|!!!
Sound|12|False|WUUUUNG!!|nowhitespace
Shichimi|15|False|MONSTER RAKSASA!
Shichimi|2|False|Aku... Aku tidak yakin akan melakukan hal ini
Shichimi|13|True|HATI-HATI!!!
Shichimi|14|True|Itu adalah ramuan

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|WAK-WAK-Waaaaw|nowhitespace
Sound|2|False|BUM!
Pepper|3|True|... hm, jadi...
Pepper|5|False|... ramuanku akan membuat orang tertawa, karena....
Pepper|4|False|apa ini saatnya giliranku?
Mayor of Komona|6|True|Lari, cepat!
Mayor of Komona|7|False|Selamatkan dirimu sekarang juga!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... seperti biasa, semuanya pergi saat giliran kita
Pepper|1|True|huh...
Pepper|4|True|Tapi aku punya ide apa yang bisa dilakukan dengan "ramuan" ini, Carrot
Pepper|5|False|... ayo bereskan semuanya dan pulang ke rumah!
Pepper|7|True|Kamu,
Pepper|8|False|Burung yang sangat besar!
Pepper|10|False|Ingin coba satu ramuan terakhir? ...
Pepper|11|False|... aku tak yakin
Pepper|6|False|HEI!
Sound|9|False|KRAK!|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ya, baca ini baik-baik...
Pepper|2|False|... aku akan menuangkan ramuan ini jika kamu tidak pergi dari Komona sekarang juga!
Mayor of Komona|3|True|Karena Pepper sudah menyelamatkan Komona,
Mayor of Komona|4|False|kami memberikan hadiah padanya, dan ramuannya adalah....??!!
Pepper|7|False|sebenarnya, ini bukan ramuan; melainkan air seni dari kucing peliharaanku saat kunjungan ke dokter hewan!
Pepper|6|True|... Haha! Benar...
Pepper|8|False|... tidak perlu dicoba, kan?
Narrator|9|False|Episode 6 : Perlombaan ramuan
Narrator|10|False|SELESAI
Writing|5|False|50.000 Ko
Credits|11|False|Maret 2015 - Ilustrasi dan gambar oleh David Revoy - Diterjemahkan oleh: Bonaventura Aditya Perdana

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot benar-benar gratis, terima kasih kepada pembaca yang telah memberikan donasinya:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Anda bisa menjadi donator Pepper&Carrot untuk episode selanjutnya:
Credits|7|False|Peralatan: dibuat dengan perangkat lunak gratis dan bebas Krita di Linux Mint
Credits|6|False|Sumber terbuka: sumber dari gambar ini tersedia di https://peppercarrot.com
Credits|5|False|Lisensi: Creative Commons Attribution Anda dapat mengubah, menjual, menerjemahkannya, dsb.
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
