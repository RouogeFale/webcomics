# Transcript of Pepper&Carrot Episode 06 [sk]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Epizóda 6: Súťaž elixírov

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Dokelu, zase som zaspala pri otvorenom okne...
Pepper|2|True|... je tu prievan ...
Pepper|3|False|... ako to, že vidím cez moje okno Komonu?
Pepper|4|False|KOMONA!
Pepper|5|False|Súťaž elixírov!
Pepper|6|True|Musela som... musela som zaspať!
Pepper|9|True|... ale?
Pepper|10|False|Kde to som ?!?
Bird|12|False|kvak?|nowhitespace
Pepper|7|False|*|nowhitespace
Note|8|False|* Viď epizóda 4 : Génius

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Ty si tak skvelý, že ti napadlo zaviezť ma na tú súťaž !
Pepper|3|False|Fan-tas-tic-ké !
Pepper|4|True|Dokonca si nezabudol ani vziať elixír, moje šaty a môj klobúk...
Pepper|5|False|... pozrime sa, aký elixír si zobral...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|ČOŽE ?!!
Mayor of Komona|3|False|Ako starosta Komony, vyhlasujem súťaž elixírov... za zahájenú!
Mayor of Komona|4|False|Naše mesto má veľkú radosť, že môžeme na našom prvom ročníku privítať tieto štyri čarodejnice !
Mayor of Komona|5|True|Poprosím vás o
Mayor of Komona|6|True|obrovský
Writing|2|False|Súťaž elixírov v Komone
Mayor of Komona|7|False|potlesk

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Ťap
Mayor of Komona|1|True|Z veľkej diaľky, až z Technologickej únie, k nám zavítala okúzľujúca a geniálna
Mayor of Komona|3|True|... nezabúdajme ani na naše domáce dievča, komonskú čarodejnicu
Mayor of Komona|5|True|... naša tretia súťažiaca pochádza z krajiny zapadajúcich mesiacov,
Mayor of Komona|7|True|... a nakoniec, je tu posledná súťažiaca, z lesov Veveričieho kraja,
Mayor of Komona|2|False|Coriander !
Mayor of Komona|4|False|Saffron !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Pepper !
Mayor of Komona|9|True|A súťaž sa môže začať!
Mayor of Komona|10|False|Hodnotiť budeme aplauz-o-metrom
Mayor of Komona|11|False|Ako prvá, Coriandrina ukážka
Coriander|13|False|... už žiaden strach zo smrti vďaka môjmu...
Coriander|14|True|... elixíru
Coriander|15|False|ZOMBIFIKÁCIE !
Audience|16|True|Ťap
Audience|17|True|Ťap
Audience|18|True|Ťap
Audience|19|True|Ťap
Audience|20|True|Ťap
Audience|21|True|Ťap
Audience|22|True|Ťap
Audience|23|True|Ťap
Audience|24|True|Ťap
Audience|25|True|Ťap
Audience|26|True|Ťap
Audience|27|True|Ťap
Audience|28|True|Ťap
Coriander|12|False|Dámy a páni...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTICKÉ !
Audience|3|True|Ťap
Audience|4|True|Ťap
Audience|5|True|Ťap
Audience|6|True|Ťap
Audience|7|True|Ťap
Audience|8|True|Ťap
Audience|9|True|Ťap
Audience|10|True|Ťap
Audience|11|True|Ťap
Audience|12|True|Ťap
Audience|13|True|Ťap
Audience|14|True|Ťap
Audience|15|True|Ťap
Audience|16|False|Ťap
Saffron|18|True|Pretože tu je
Saffron|17|True|... ale prosím vás, šetrite si svoj potlesk obyvatelia Komony !
Saffron|22|False|... vzbudí v nich žiarlivosť!
Saffron|19|True|môj
Saffron|25|False|LUXUSNOSTI !
Saffron|24|True|... elixíru
Saffron|23|False|... všetko je to možné vďaka jednoduchej aplikácii jedinej kvapky môjho ...
Audience|26|True|Ťap
Audience|27|True|Ťap
Audience|28|True|Ťap
Audience|29|True|Ťap
Audience|30|True|Ťap
Audience|31|True|Ťap
Audience|32|True|Ťap
Audience|33|True|Ťap
Audience|34|True|Ťap
Audience|35|True|Ťap
Audience|36|True|Ťap
Audience|37|True|Ťap
Audience|38|True|Ťap
Audience|39|True|Ťap
Audience|40|False|Ťap
Mayor of Komona|42|False|Tento elixír spraví z obyvateľov Komony boháčov!
Mayor of Komona|41|True|Fantastické! Neuveriteľné!
Audience|44|True|Ťap
Audience|45|True|Ťap
Audience|46|True|Ťap
Audience|47|True|Ťap
Audience|48|True|Ťap
Audience|49|True|Ťap
Audience|50|True|Ťap
Audience|51|True|Ťap
Audience|52|True|Ťap
Audience|53|True|Ťap
Audience|54|True|Ťap
Audience|55|True|Ťap
Audience|56|True|Ťap
Audience|57|True|Ťap
Audience|58|True|Ťap
Audience|59|True|Ťap
Audience|60|False|Ťap
Mayor of Komona|2|False|Coriander porazila smrť so svojim záz-rač-ným elixírom!
Saffron|21|True|Elixír, na ktorý ste všetci čakali: ten ktorý ohúri všetkých vašich blízkych ...
Mayor of Komona|43|False|Váš aplauz sa nemýli. Coriander je skutočne vyradená z hry
Saffron|20|False|elixír !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Pre Shichimi bude teraz naozaj ťažké zabodovať svojou prezentáciou !
Shichimi|4|True|NIE!
Shichimi|5|True|Ja nemôžem, je to príliš nebezpečné
Shichimi|6|False|PREPÁČTE !
Mayor of Komona|3|False|... no tak Shichimi, každý čaká len na teba
Mayor of Komona|7|False|Vyzerá to tak, dámy a páni že Shichimi odstupuje...
Saffron|8|False|Daj mi to !
Saffron|9|False|... a prestaň predstierať hanblivosť, kazíš tým celú šou
Saffron|10|False|Každý už predsa vie, že ja som tú súťaž vyhrala ... bezohľadu na tvoj elixír ...
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|OBROVSKÝCH PRÍŠER !
Shichimi|2|False|Ja... Ja som nevedela že to budeme aj demonštrovať
Shichimi|13|True|OPATRNE!!!
Shichimi|14|True|To je elixír

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|KVA-KVA-KV aa aa aa aaaa kk|nowhitespace
Sound|2|False|BUCH!
Pepper|3|True|... ha, skvelé !
Pepper|5|False|... môj elixír bude stáť aspoň za poriadnu dávku smiechu, pretože
Pepper|4|False|takže som na rade ?
Mayor of Komona|6|True|Bež,hlupaňa!
Mayor of Komona|7|False|Súťaž sa skončila ! ... zachráň sa !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... ako obyčajne, každý odchádza práve vtedy, keď sme na rade my
Pepper|1|True|tu to máme ...
Pepper|4|False|...dáme to tu všetko do poriadku a ide sa domov !
Pepper|6|True|Ty!
Pepper|7|False|Prerastený-luxusný-zombi-kanárik!
Pepper|9|True|Chceš vyskúšať posledný elixír? ...
Pepper|10|False|... však nie, hm ?
Pepper|5|False|HEJ !
Sound|8|False|ŽUCH !
Pepper|3|True|Konečne mám ale nápad, ako môžeme využiť tvoj skvelý výber "elixíru", Carrot

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Áno, prečítaj si pozorne štítok ...
Pepper|2|False|... Nebudem rozmýšľať dvakrát, kým to na teba celé vylejem, ak okamžite neopustíš Komonu !
Mayor of Komona|3|True|Pretože zachránila naše mesto v čase nebezpečenstva,
Mayor of Komona|4|False|udeľujeme prvé miesto Pepper za jej elixír ... ??!!
Pepper|7|False|... heh... vlastne, ono to nie je celkom elixír; je to vzorka moču mojej mačky z poslednej návštevy veterinára !
Pepper|6|True|... Haha! jasné ...
Pepper|8|False|... takže žiadna ukážka ?...
Narrator|9|False|Epizóda 6 : Súťaž elixírov
Narrator|10|False|KONIEC
Writing|5|False|50,000 Ko
Credits|11|False|Marec 2015 - Nakreslil a napísal : David Revoy - Preklad : talime

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot je úplne zdarma, open-source a sponzorovaný vďaka podpore čitateľov. Za túto epizódu chcem poďakovať týmto 245 sponzorom :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Aj ty sa môžeš stať sponzorom ďalšej epizódy komiksu Pepper&Carrot :
Credits|7|False|Nástroje : Táto epizóda bola 100% nakreslená pomocou voľne dostupného open-source softvéru Krita na Linuxe Mint
Credits|6|False|Open-source : všetky zdrojové súbory s vrstvami a fontmi sú dostupné na oficiálnej stránke
Credits|5|False|Licencia : Creative Commons Attribution Môžeš upravovať, zdieľať, predávať atď. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
