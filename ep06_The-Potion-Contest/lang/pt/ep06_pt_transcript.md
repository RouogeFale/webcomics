# Transcript of Pepper&Carrot Episode 06 [pt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episódio 6: O Torneio de Poções

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Puxa vida, caí no sono com a janela aberta outra vez...
Pepper|2|True|...e tá ventando tanto...
Pepper|3|False|...e por que eu estou vendo Komona pela janela?
Pepper|4|False|KOMONA!
Pepper|5|False|O torneio de poções!
Pepper|6|True|Eu devo... devo ter cochilado sem querer!
Pepper|9|True|...mas?
Pepper|10|False|Onde eu estou?!?
Bird|12|False|cK?|nowhitespace
Bird|11|True|qua|nowhitespace
Pepper|7|False|*
Note|8|False|* Ver episódio 4 : Ideia de Gênio

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Você é um fofo por ter me trazido até o torneio!
Pepper|3|False|Fan-tás-ti-co!
Pepper|4|True|Você inclusive trouxe uma poção, minhas roupas e meu chapéu...
Pepper|5|False|...vamos ver que poção você trouxe...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|O QUÊ?!!
Mayor of Komona|3|False|Como prefeito de Komona, tenho o prazer de dar início às competições do torneio de poções!
Mayor of Komona|4|False|É um grande prazer para nossa cidade receber não menos que quatro bruxas para a primeira edição deste torneio.
Mayor of Komona|5|True|Por favor, uma
Mayor of Komona|6|True|enorme
Writing|2|False|Torneio de Poções de Komona
Mayor of Komona|7|False|salva de palmas...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Mayor of Komona|1|True|Da longínqua União Tecnologica, é uma honra dar as boas-vindas à encantadora e engenhosa,
Mayor of Komona|3|True|...não vamos esquecer nossa garota, a própria bruxa de Komona,
Mayor of Komona|5|True|...nossa terceira participante vem nos visitar das terras das luas poentes,
Mayor of Komona|7|True|...e finalmente, nossa última participante, da floresta do Canto do Esquilo,
Mayor of Komona|2|False|Coriander!
Mayor of Komona|4|False|Saffron!
Mayor of Komona|6|False|Shichimi!
Mayor of Komona|8|False|Pepper!
Mayor of Komona|9|True|Que os jogos comecem!
Mayor of Komona|10|False|A votação se dará pelo aplausômetro
Mayor of Komona|11|False|Primeiramente, a demonstração da Coriander
Coriander|13|False|...não temam mais a morte, graças a minha...
Coriander|14|True|...Poção
Coriander|15|False|ZUMBIFICADORA!
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Coriander|12|False|Senhoras e Senhores...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTÁSTICO !
Audience|3|True|Clap
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|False|Clap
Saffron|18|True|...Pois lá vem a
Saffron|17|True|... mas por favor, guardem seus aplausos, povo de Komona!
Saffron|22|False|...deixá-los com inveja!
Saffron|19|True|MINHA POÇÃO!
Saffron|25|False|CHIQUEZA!
Saffron|24|True|... Poção da
Saffron|23|False|...tudo isso é possível com a simples aplicação de uma única gota da minha...
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|False|Clap
Mayor of Komona|42|False|Essa poção poderia tornar todos em Komona ricos!
Mayor of Komona|41|True|Fantástico! Incrível!
Audience|44|True|Clap
Audience|45|True|Clap
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Mayor of Komona|2|False|Coriander desafia a própria morte com essa poção mi-la-gro-sa!
Saffron|21|True|A poção pela qual todos vocês estavam esperando: aquela que vai maravilhar seus vizinhos...
Mayor of Komona|43|False|Seus aplausos não podem estar errados. Coriander já foi eliminada

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Vai ser difícil para a Shichimi ganhar depois dessa última apresentação!
Shichimi|4|True|NÃO !
Shichimi|5|True|Eu não posso, é muito perigoso
Shichimi|6|False|DESCULPE !
Mayor of Komona|3|False|...vamos lá Shichimi, tá todo mundo esperando você
Mayor of Komona|7|False|Então, Senhoras & Senhores parece que Shichimi desiste...
Saffron|8|False|Me dá isso!
Saffron|9|False|...e pare de se fingir de tímida, você está estragando o show
Saffron|10|False|Todo mundo já sabe que eu ganhei o torneio, então não importa o que sua poção faz...
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|MONSTROS GIGANTES!
Shichimi|2|False|Eu... Eu não sabia que precisaríamos demonstrar nossas poções
Shichimi|13|True|CUIDADO!!!
Shichimi|14|True|É uma poção que cria

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CAW-CAW-C aa aa aa aa w w ww|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|...ah, legal!
Pepper|5|False|...pelo menos minha poção vai causar algumas risadas....
Pepper|4|False|Então, é minha vez agora?
Mayor of Komona|6|True|Corre sua doida!
Mayor of Komona|7|False|A competição acabou! ...Salve-se!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...como sempre, todo mundo vai embora bem na nossa vez
Pepper|1|True|Ai ai...
Pepper|4|True|Pelo menos eu tenho uma ideia do que fazer com a sua "poção", Carrot...
Pepper|5|False|...arrumar as coisas por aqui e voltar pra casa!
Pepper|7|True|Seu
Pepper|8|False|Canário-zumbi-chique-desproporcional!
Pepper|10|False|Quer experimentar uma última poção?...
Pepper|11|False|...não tá muito afim, é?
Pepper|6|False|EI!
Sound|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|É... Dá uma olhada no rótulo, com cuidado...
Pepper|2|False|...Eu não vou pensar duas vezes antes de derramar isso em você se você não for embora de Komona agora!
Mayor of Komona|3|True|Visto que ela salvou nossa cidade quando estávamos em perigo,
Mayor of Komona|4|False|nós entregamos o primeiro prêmio a Pepper por sua Poção de ...??!!
Pepper|7|False|...hum... ...na verdade, não é bem uma poção, é uma amostra de urina do meu gato, de sua última visita ao veterinário!
Pepper|6|True|...Haha! pois é...
Pepper|8|False|...sem demonstração então?...
Narrator|9|False|Episódio 6: O Torneio de Poções
Narrator|10|False|FIM
Writing|5|False|50,000 Ko
Credits|11|False|Março 2015 - Arte por roteiro por David Revoy - Tradução por Frederico Batista

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot é inteiramente livre, open-source e patrocinado graças a generosa contribuição dos leitores. Para esse episódio, obrigado aos 245 Patronos :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Você também pode ser tornar um patrono de Pepper&Carrot para o próximo episódio:
Credits|7|False|Ferramentas : Esse episódio foi 100% desenhado com o software Grátis/Livre Krita no Linux Mint
Credits|6|False|Open-source : todos os arquivos fonte com layers e fontes, estão disponíveis no site oficial
Credits|5|False|Licença : Creative Commons Attribution Você pode modificar, distribuir, vender etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
