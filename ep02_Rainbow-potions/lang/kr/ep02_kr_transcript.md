# Transcript of Pepper&Carrot Episode 02 [kr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|제2 화: 무지개 물약

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|쪼롱
Sound|6|True|쪼롱
Sound|7|False|쪼롱
Writing|1|True|경고
Writing|2|True|마녀사유지
Writing|3|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|톡|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|쪼록
Sound|2|True|쪼록
Sound|3|False|쪼록

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|꿀꺽
Sound|2|False|꿀꺽
Sound|3|True|꿀꺽
Sound|4|False|꿀꺽
Sound|5|True|꿀꺽
Sound|6|False|꿀꺽
Sound|9|False|읍
Sound|7|False|풉!
Sound|8|False|주르르륵

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|뽀글
Sound|2|False|뽀글
Sound|4|True|퍽|nowhitespace
Sound|3|True|철
Sound|6|True|퍽|nowhitespace
Sound|5|True|철
Sound|8|False|퍽|nowhitespace
Sound|7|True|철

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|이 웹툰은 오픈소스이며 이번 화는 아래 주소에서 21명의 후원자의 도움을 받아 제작되었습니다.
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|이분들에게 감사의 말씀을 전합니다.
Credits|4|False|GNU/리눅스에서 Krita로 제작
