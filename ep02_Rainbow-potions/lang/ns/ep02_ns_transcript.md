# Transcript of Pepper&Carrot Episode 02 [ns]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Folg 2: De kakelbunten Töverdrunks

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Gluck
Sound|6|True|Gluck
Sound|7|False|Gluck
Writing|1|True|Wohrschau!
Writing|3|False|Privaat!
Writing|2|True|HEX
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|klack|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Blupp
Sound|2|True|Blupp
Sound|3|False|Blupp

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Gluck
Sound|2|False|Gluck
Sound|3|True|Gluck
Sound|4|False|Gluck
Sound|5|True|Gluck
Sound|6|False|Gluck
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M|nowhitespace
Sound|7|True|B|nowhitespace
Sound|8|True|u|nowhitespace
Sound|9|True|ä|nowhitespace
Sound|10|False|rgh!|nowhitespace
Sound|11|True|P|nowhitespace
Sound|12|True|l|nowhitespace
Sound|13|True|a|nowhitespace
Sound|14|True|t|nowhitespace
Sound|15|True|s|nowhitespace
Sound|16|True|c|nowhitespace
Sound|17|False|h|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Blupp
Sound|2|True|B
Sound|3|False|lupp|nowhitespace
Sound|6|True|sch|nowhitespace
Sound|5|True|at|nowhitespace
Sound|4|True|p
Sound|9|True|sch|nowhitespace
Sound|8|True|at|nowhitespace
Sound|7|True|p
Sound|12|False|sch|nowhitespace
Sound|11|True|at|nowhitespace
Sound|10|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Düsse Webcomic is Open Source un düsse Folg is maakt worrn mit de Help vun
Credits|3|False|www.patreon.com/davidrevoy
Credits|4|False|Velen Dank an:
Credits|5|False|maakt mit Krita op GNU/Linux
Credits|2|True|mien 21 Förderer op
